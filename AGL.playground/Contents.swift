import Foundation
import PlaygroundSupport
import XCTest

//Allow Playground to run async code (times out after 30 seconds)
PlaygroundPage.current.needsIndefiniteExecution = true
//: ## Data structures
//: Data structures for working with the API.
public struct Person: Decodable {
  let gender:Gender
  let pets:[Pet]?
}

public enum Gender: String, Decodable {
  case female = "Female", male = "Male"
}

public struct Pet: Decodable {
  let name:String
  let type:AnimalType
  
  enum AnimalType: String, Decodable {
    case cat = "Cat", dog = "Dog", fish = "Fish"
  }
}

extension Pet: Equatable, CustomStringConvertible {
  public static func ==(lhs: Pet, rhs: Pet) -> Bool {
    return lhs.name == rhs.name && lhs.type == rhs.type
  }
  
  public var description: String {
    return name
  }
}

/// Abstraction to handle API call results. Use in association with `ResultCallback`.
///
/// - success: API call was successful, and may have an associated value.
/// - error: API call failed, with optional error.
public enum Result {
  case success(Any?), error(Error?)
  
  /// Get the associated value with self, or nil.
  ///
  /// - Returns: Value associated with self, or nil if no value associated.
  func getValue()-> Any? {
    switch self {
    case .success(let value):
      return value
    case .error(let error):
      return error
    }
  }
  
  /// Booleon represention of current `Result`
  var isSuccess:Bool {
    switch self {
    case .success(_):
      return true
    case .error(_):
      return false
    }
  }
}

//Type alias for API callbacks
public typealias ResultCallback = (Result) -> ()
//: ## Logic
//: Calls the API for data, and then sorts accordingly before outputting to the console.
// Makes a GET call to retrieve people and their pets. Then finds all cats, and sorts them by owner gender and then alphabetically ascending by name.
// Note: In real project, ideally would break this out further and use dependency injection or use framework such as OHHTTPStubs to stub for testing.
public func findCats(callback:ResultCallback? = nil) {
  guard let url = URL(string: "https://agl-developer-test.azurewebsites.net/people.json") else { return }
  let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
    guard
      error == nil,
      (response as? HTTPURLResponse)?.statusCode == 200,
      let data = data else {
        DispatchQueue.main.async { callback?(Result.error(error)) }
        return
    }
    
    //Now decode the JSON & get cats
    let result:Result
    do {
      let people = try JSONDecoder().decode([Person].self, from: data)
      result = Result.success(people.getCatsByOwnerGender())
    } catch let error {
      result = Result.error(error)
      return
    }
    
    DispatchQueue.main.async { callback?(result) }
  }
  
  task.resume()
}

fileprivate extension Sequence where Iterator.Element == Person {
  /// Sorts through each `Person`'s `Pet` array, and finds cats. Then groups by the owner's gender.
  ///
  /// - Returns: All cats found, sorted alphabetically in ascending order under the gender
  func getCatsByOwnerGender() -> [String:[Pet]]{
    var results = [String:[Pet]]()
    
    forEach {
      guard let pets = $0.pets, let cats = pets.find(animal: .cat) else { return }
      
      //Check if cats for gender already exist and append
      let gender = $0.gender.rawValue
      if let _ = results[gender] {
        results[gender]?.append(contentsOf: cats)
      } else {
        results[gender] = cats
      }
    }
    
    //Loop through dictionary, and sort alphabetically ascending A-Z
    for (name, value) in results {
      results[name] = value.sorted { $0.name < $1.name }
    }
    
    return results
  }
}

fileprivate extension Sequence where Iterator.Element == Pet {
  func find(animal animalType: Pet.AnimalType) -> [Pet]? {
    let animals = filter { $0.type == animalType }
    return animals.count > 0 ? animals : nil
  }
}
//: ## Unit tests
//: Unit test coverage for the sorting functionality.
class TestCases: XCTestCase {
  var people = [Person]()
  
  override func setUp() {
    super.setUp()
    guard let data = """
[{"name":"Bob","gender":"Male","age":23,"pets":[{"name":"Garfield","type":"Cat"},{"name":"Fido","type":"Dog"}]},{"name":"Jennifer","gender":"Female","age":18,"pets":[{"name":"Garfield","type":"Cat"}]},{"name":"Steve","gender":"Male","age":45,"pets":null},{"name":"Fred","gender":"Male","age":40,"pets":[{"name":"Tom","type":"Cat"},{"name":"Max","type":"Cat"},{"name":"Sam","type":"Dog"},{"name":"Jim","type":"Cat"}]},{"name":"Samantha","gender":"Female","age":40,"pets":[{"name":"Tabby","type":"Cat"}]},{"name":"Alice","gender":"Female","age":64,"pets":[{"name":"Simba","type":"Cat"},{"name":"Nemo","type":"Fish"}]}]
""".data(using: .utf8),
      let json = try? JSONDecoder().decode([Person].self, from: data) else { return }
    people = json
  }
  
  func testSorting(){
    let genderCats = people.getCatsByOwnerGender()
    guard let maleCats = genderCats["Male"], let femaleCats = genderCats["Female"] else { XCTFail("Male & female owners of cats should exist"); return }
    
    XCTAssertEqual(genderCats.count, 2)
    XCTAssertEqual(maleCats.count, 4)
    XCTAssertEqual(femaleCats.count, 3)
    
    //Test male alphabetical arrangement
    XCTAssertEqual(maleCats[0], Pet(name: "Garfield", type: .cat))
    XCTAssertEqual(maleCats[1], Pet(name: "Jim", type: .cat))
    XCTAssertEqual(maleCats[2], Pet(name: "Max", type: .cat))
    XCTAssertEqual(maleCats[3], Pet(name: "Tom", type: .cat))
    
    //Test female alphabetical arrangement
    XCTAssertEqual(femaleCats[0], Pet(name: "Garfield", type: .cat))
    XCTAssertEqual(femaleCats[1], Pet(name: "Simba", type: .cat))
    XCTAssertEqual(femaleCats[2], Pet(name: "Tabby", type: .cat))
  }
  
  func testSortingEmptyPets(){
    let noPetPerson = Person(gender: .male, pets: nil)
    XCTAssertEqual([noPetPerson].getCatsByOwnerGender().count, 0)
  }
  
  func testAnimalFilter(){
    let pets = [Pet(name: "Garfish", type: .fish), Pet(name: "Gardog", type: .dog), Pet(name: "Garfield", type: .cat)]
    guard let cats = pets.find(animal: .fish) else {
      XCTFail("Cats should not be nil")
      return
    }
    
    XCTAssertEqual(cats.count, 1)
    XCTAssertEqual(cats.first, Pet(name: "Garfish", type: .fish))
    
    let noPets = [Pet]()
    let noAnimals = noPets.find(animal: .cat)
    XCTAssertNil(noAnimals)
  }
  
  func testPerformance(){
    measure {
      _ = people.getCatsByOwnerGender()
    }
  }
}
//: ## Run the sample
//: Comment out the code below to run unit tests or just show the output.
//Run the unit tests
TestCases.defaultTestSuite.run()

//Run the logic to show output
findCats {
  guard $0.isSuccess, let cats = $0.getValue() as? [String: [Pet]] else {
    print("Error retrieving cats: \($0.getValue() ?? "No error provided")")
    return
  }
  
  for (key, value) in cats {
    print("\n\(key)")
    value.forEach { print("- \($0)", separator: "\n") }
  }
}
