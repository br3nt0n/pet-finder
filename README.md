#  AGL Code Test (iOS developer role)
This Playground file is for the AGL iOS developer role code test. It downloads a list of people and their pets, and filters out all animals aside from cats. The results are then output to the Playground's console.

### System Requirements
* Xcode 9 (beta 3 or above)
* Swift 4 (beta)

### Getting Started
The easiest way to run this sample is by downloading and installing the latest **[Xcode 9 beta](https://developer.apple.com/download/)**. The Xcode 9 beta is easiest to use because the code implemented uses Swift 4's `Codeable` API to parse JSON.

To run the playground, ensure the Debug area is shown at the bottom of the screen and click the play icon. Click stop once all results have been output to the console, or wait 30 seconds for the Playground to automatically terminate the process.

Unit tests also output the results to the console.

### Example output
Male
- Garfield
- Jim
- Max
- Tom

Female
- Garfield
- Simba
- Tabby
